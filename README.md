#An example Java project with JUnit tests.

This project uses [Maven](https://maven.apache.org/) to manage dependencies, so you don't have to go and download [JUnit](http://junit.org/) and all its dependencies manually.

Once you have installed Maven, you can run `mvn test` to run the tests.

If you are using an IDE like IntelliJ, you'll be able to run the tests from there, too.