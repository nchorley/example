package com.example;

import org.junit.Test;
import static org.junit.Assert.*;

public class MaximumWordLengthTest {
    @Test
    public void testEmptyString() {
        String s = "";

        int maxWordLength = Example.maxWordLength(s);

        assertEquals(0, maxWordLength);
    }

    @Test
    public void testStringContainingOneWord() {
        String s = "the";

        int maxWordLength = Example.maxWordLength(s);

        assertEquals(3, maxWordLength);
    }

    @Test
    public void testStringContainingMultipleWords() {
        String s = "tomorrow is Friday";

        int maxWordLength = Example.maxWordLength(s);

        assertEquals(8, maxWordLength);
    }
}
